#!/bin/sh

if [ "$1" == "clean" ]; then
	if [ -f "Makefile" ]; then
		make clean
		make distclean
	else
		echo "No Makefile: it seems that project is already cleaned"
	fi
else
	qmake-qt4
	make
fi
