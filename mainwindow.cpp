/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>
#include <QTreeWidget>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>
#include <qwt_legend.h>

#include "mainwindow.h"
#include "treemodel.h"

MainWindow::MainWindow()
{
    Q_INIT_RESOURCE(simpletreemodel);
    QFile file(":/default.txt");
    file.open(QIODevice::ReadOnly);
    model = new TreeModel(trUtf8(file.readAll()));
    file.close();

    QWidget *widget = new QWidget;
    setCentralWidget(widget);

    QWidget *topFiller = new QWidget;
    topFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    //QTreeView view;
    QTreeView * treeView = new QTreeView(this);
    treeView->setModel(model);
    treeView->header()->setResizeMode(0,QHeaderView::ResizeToContents);
    connect(treeView, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(checkPointWindow()));
    for( int i = 0; i < model->rowCount(); i++ ) {
        QModelIndex sourceIndex = model->index( i, 0 );
        //QModelIndex mappedIndex = model->mapFromSource( sourceIndex );
        if( !sourceIndex.isValid() )
            continue;
        treeView->setExpanded( sourceIndex, true );
    }
    //view.setWindowTitle(QObject::tr("Simple Tree Model"));
    //view.show();

#ifdef Q_OS_SYMBIAN
    infoLabel = new QLabel(tr("<i>Choose a menu option</i>"));
#else
    infoLabel = new QLabel(tr("<i>Choose a menu option, or right-click to "
                              "invoke a context menu</i>"));
#endif
    infoLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    infoLabel->setAlignment(Qt::AlignCenter);

    QWidget *bottomFiller = new QWidget;
    bottomFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(5);
//  layout->addWidget(topFiller);
    layout->addWidget(treeView);
//  layout->addWidget(infoLabel);
//  layout->addWidget(bottomFiller);
    widget->setLayout(layout);

    createActions();
    createMenus();

#ifndef Q_OS_SYMBIAN
    QString message = tr("A context menu is available by right-clicking");
    statusBar()->showMessage(message);
#endif

    setWindowTitle(tr("Visualizer"));
    //setMinimumSize(160, 160);
    //resize(480, 320);
    setWindowState(Qt::WindowMaximized);
}

void MainWindow::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);
    menu.addAction(cutAct);
    menu.addAction(copyAct);
    menu.addAction(pasteAct);
    menu.exec(event->globalPos());
}

void MainWindow::checkPointWindow()
{
#ifndef Q_OS_SYMBIAN
    /*QTreeView view;
    view.setModel(&model);
    view.setWindowTitle(QObject::trUtf8("Контрольные точки"));
    view.show();*/
    QFile file(":/breakpoints.txt");
    file.open(QIODevice::ReadOnly);
    bp_model = new TreeModel(trUtf8(file.readAll()));
    file.close();

    QWidget *wdg = new QWidget;
    QTreeView * SlaveView = new QTreeView(wdg);
    SlaveView->setModel(bp_model);
    SlaveView->header()->setResizeMode(0,QHeaderView::ResizeToContents);
    connect(SlaveView, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(plotWindow()));
    //SlaveView->setWindowTitle(QObject::trUtf8("Контрольные точки"));
    //SlaveView->show();
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(5);
//  layout->addWidget(topFiller);
    layout->addWidget(SlaveView);
//  layout->addWidget(infoLabel);
//  layout->addWidget(bottomFiller);
    wdg->setLayout(layout);
    wdg->resize( 600, 200 );
    wdg->setWindowTitle(QObject::trUtf8("Контрольные точки"));
    wdg->show();
#endif
}

void MainWindow::plotWindow() {
    QWidget *wdg = new QWidget;

    QwtPlot* plot = new QwtPlot();
    //plot.setTitle( trUtf8("doLog") );
    plot->setCanvasBackground( Qt::white );
    plot->setAxisScale( QwtPlot::yLeft, 3.0, 10.0);
    //plot->insertLegend( new QwtLegend() );

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->attach( plot );

    QwtPlotCurve *curve = new QwtPlotCurve();
    curve->setTitle( QObject::trUtf8("genericSolver") );
    curve->setPen( Qt::blue, 4 ),
    curve->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QwtSymbol *symbol = new QwtSymbol( QwtSymbol::Ellipse,
    QBrush( Qt::yellow ), QPen( Qt::red, 2 ), QSize( 8, 8 ) );
    curve->setSymbol( symbol );

    QPolygonF points;
    points << QPointF( 0.0, 4.8 ) << QPointF( 1.0, 5.2 )
        << QPointF( 2.0, 4.5 ) << QPointF( 3.0, 6.4 )
        << QPointF( 4.0, 7.9 ) << QPointF( 5.0, 8.3 ) << QPointF( 6.0, 8.6 ) << QPointF( 7.0, 8.0 ) << QPointF( 8.0, 7.7 ) << QPointF( 9.0, 6.9 ) << QPointF( 10.0, 7.4 );
    curve->setSamples( points );

    curve->attach( plot );

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(5);
//  layout->addWidget(topFiller);
    layout->addWidget(plot);
//  layout->addWidget(infoLabel);
//  layout->addWidget(bottomFiller);
    wdg->setLayout(layout);
    wdg->resize( 600, 400 );
    wdg->setWindowTitle(QObject::trUtf8("Визуализация параметра состояния"));
    wdg->show();
}

void MainWindow::newFile()
{
    //infoLabel->setText(tr("Invoked <b>File|New</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked File->New"));
#endif
}

void MainWindow::open()
{
//    infoLabel->setText(tr("Invoked <b>File|Open</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked File->Open"));
#endif
}

void MainWindow::save()
{
//    infoLabel->setText(tr("Invoked <b>File|Save</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked File->Save"));
#endif
}

void MainWindow::print()
{
//    infoLabel->setText(tr("Invoked <b>File|Print</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked File->Print"));
#endif
}

void MainWindow::undo()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Undo</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Undo"));
#endif
}

void MainWindow::redo()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Redo</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Redo"));
#endif
}

void MainWindow::cut()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Cut</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Cut"));
#endif
}

void MainWindow::copy()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Copy</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Copy"));
#endif
}

void MainWindow::paste()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Paste</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Paste"));
#endif
}

void MainWindow::bold()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Format|Bold</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Format->Bold"));
#endif
}

void MainWindow::italic()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Format|Italic</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Format->Italic"));
#endif
}

void MainWindow::leftAlign()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Format|Left Align</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Format->Left Align"));
#endif
}

void MainWindow::rightAlign()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Format|Right Align</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Format->Right Align"));
#endif
}

void MainWindow::justify()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Format|Justify</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Format->Justify"));
#endif
}

void MainWindow::center()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Format|Center</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Format->Center"));
#endif
}

void MainWindow::setLineSpacing()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Format|Set Line Spacing</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Format->Set Line Spacing"));
#endif
}

void MainWindow::setParagraphSpacing()
{
//    infoLabel->setText(tr("Invoked <b>Edit|Format|Set Paragraph Spacing</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Edit->Format->Set Paragraph Spacing"));
#endif
}

void MainWindow::about()
{
//    infoLabel->setText(tr("Invoked <b>Help|About</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Help->About"));
#endif
    QMessageBox::about(this, tr("About Menu"),
            tr("The <b>Menu</b> example shows how to create "
               "menu-bar menus and context menus."));
}

void MainWindow::aboutQt()
{
//    infoLabel->setText(tr("Invoked <b>Help|About Qt</b>"));
#ifndef Q_OS_SYMBIAN
    statusBar()->showMessage(tr("Invoked Help->About Qt"));
#endif
}

void MainWindow::createActions()
{
    newAct = new QAction(tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new file"));
    connect(newAct, SIGNAL(triggered()), this, SLOT(newFile()));

    openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document to disk"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    printAct = new QAction(tr("&Print..."), this);
    printAct->setShortcuts(QKeySequence::Print);
    printAct->setStatusTip(tr("Print the document"));
    connect(printAct, SIGNAL(triggered()), this, SLOT(print()));

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    undoAct = new QAction(tr("&Undo"), this);
    undoAct->setShortcuts(QKeySequence::Undo);
    undoAct->setStatusTip(tr("Undo the last operation"));
    connect(undoAct, SIGNAL(triggered()), this, SLOT(undo()));

    redoAct = new QAction(tr("&Redo"), this);
    redoAct->setShortcuts(QKeySequence::Redo);
    redoAct->setStatusTip(tr("Redo the last operation"));
    connect(redoAct, SIGNAL(triggered()), this, SLOT(redo()));

    cutAct = new QAction(tr("Cu&t"), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    cutAct->setStatusTip(tr("Cut the current selection's contents to the "
                            "clipboard"));
    connect(cutAct, SIGNAL(triggered()), this, SLOT(cut()));

    copyAct = new QAction(tr("&Copy"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    copyAct->setStatusTip(tr("Copy the current selection's contents to the "
                             "clipboard"));
    connect(copyAct, SIGNAL(triggered()), this, SLOT(copy()));

    pasteAct = new QAction(tr("&Paste"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                              "selection"));
    connect(pasteAct, SIGNAL(triggered()), this, SLOT(paste()));

    boldAct = new QAction(tr("&Bold"), this);
    boldAct->setCheckable(true);
    boldAct->setShortcut(QKeySequence::Bold);
    boldAct->setStatusTip(tr("Make the text bold"));
    connect(boldAct, SIGNAL(triggered()), this, SLOT(bold()));

    QFont boldFont = boldAct->font();
    boldFont.setBold(true);
    boldAct->setFont(boldFont);

    italicAct = new QAction(tr("&Italic"), this);
    italicAct->setCheckable(true);
    italicAct->setShortcut(QKeySequence::Italic);
    italicAct->setStatusTip(tr("Make the text italic"));
    connect(italicAct, SIGNAL(triggered()), this, SLOT(italic()));

    QFont italicFont = italicAct->font();
    italicFont.setItalic(true);
    italicAct->setFont(italicFont);

    setLineSpacingAct = new QAction(tr("Set &Line Spacing..."), this);
    setLineSpacingAct->setStatusTip(tr("Change the gap between the lines of a "
                                       "paragraph"));
    connect(setLineSpacingAct, SIGNAL(triggered()), this, SLOT(setLineSpacing()));

    setParagraphSpacingAct = new QAction(tr("Set &Paragraph Spacing..."), this);
    setLineSpacingAct->setStatusTip(tr("Change the gap between paragraphs"));
    connect(setParagraphSpacingAct, SIGNAL(triggered()),
            this, SLOT(setParagraphSpacing()));

    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    aboutQtAct = new QAction(tr("About &Qt"), this);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(aboutQtAct, SIGNAL(triggered()), this, SLOT(aboutQt()));

    leftAlignAct = new QAction(tr("&Left Align"), this);
    leftAlignAct->setCheckable(true);
    leftAlignAct->setShortcut(tr("Ctrl+L"));
    leftAlignAct->setStatusTip(tr("Left align the selected text"));
    connect(leftAlignAct, SIGNAL(triggered()), this, SLOT(leftAlign()));

    rightAlignAct = new QAction(tr("&Right Align"), this);
    rightAlignAct->setCheckable(true);
    rightAlignAct->setShortcut(tr("Ctrl+R"));
    rightAlignAct->setStatusTip(tr("Right align the selected text"));
    connect(rightAlignAct, SIGNAL(triggered()), this, SLOT(rightAlign()));

    justifyAct = new QAction(tr("&Justify"), this);
    justifyAct->setCheckable(true);
    justifyAct->setShortcut(tr("Ctrl+J"));
    justifyAct->setStatusTip(tr("Justify the selected text"));
    connect(justifyAct, SIGNAL(triggered()), this, SLOT(justify()));

    centerAct = new QAction(tr("&Center"), this);
    centerAct->setCheckable(true);
    centerAct->setShortcut(tr("Ctrl+E"));
    centerAct->setStatusTip(tr("Center the selected text"));
    connect(centerAct, SIGNAL(triggered()), this, SLOT(center()));

    alignmentGroup = new QActionGroup(this);
    alignmentGroup->addAction(leftAlignAct);
    alignmentGroup->addAction(rightAlignAct);
    alignmentGroup->addAction(justifyAct);
    alignmentGroup->addAction(centerAct);
    leftAlignAct->setChecked(true);
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(trUtf8("&Файл"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    editMenu = menuBar()->addMenu(trUtf8("&Многопроцессорная система"));
    editMenu->addAction(undoAct);
    editMenu->addAction(redoAct);
    editMenu->addSeparator();
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);
    editMenu->addSeparator();

    helpMenu = menuBar()->addMenu(trUtf8("&Настройки"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);

    formatMenu = editMenu->addMenu(tr("&Format"));
    formatMenu->addAction(boldAct);
    formatMenu->addAction(italicAct);
    formatMenu->addSeparator()->setText(tr("Alignment"));
    formatMenu->addAction(leftAlignAct);
    formatMenu->addAction(rightAlignAct);
    formatMenu->addAction(justifyAct);
    formatMenu->addAction(centerAct);
    formatMenu->addSeparator();
    formatMenu->addAction(setLineSpacingAct);
    formatMenu->addAction(setParagraphSpacingAct);
}
